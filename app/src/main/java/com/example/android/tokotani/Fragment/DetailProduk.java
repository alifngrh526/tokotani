package com.example.android.tokotani.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.tokotani.HalamanUtama;
import com.example.android.tokotani.R;
import com.example.android.tokotani.adapter.AdapterBarangBeli;
import com.example.android.tokotani.adapter.PembeliList;
import com.example.android.tokotani.model.Produk;
import com.example.android.tokotani.model.Transaksi;
import com.example.android.tokotani.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailProduk extends AppCompatActivity {
    ImageView fotoDetail;
    TextView nmBarang, hrgaBarang, stokBarang, deskripProduk;
    ListView pembeli;
    //Button btEdit;
    List<User> listPembeli;
    private DatabaseReference mDatabaseReference;
    private DatabaseReference mDatabaseReference1;
    private AdapterBarangBeli mAdapter;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);

        nmBarang = findViewById(R.id.judulDetail);
        fotoDetail = findViewById(R.id.detailFoto);
        hrgaBarang = findViewById(R.id.hargaDetail);
        stokBarang = findViewById(R.id.stokDetail);
        deskripProduk = findViewById(R.id.deskripsiDetail);
        Button btn_edit = findViewById(R.id.btn_edit);
        pembeli = findViewById(R.id.listViewPembeli);

        final String namaproduk = getIntent().getStringExtra("nmProduk");
        final String hargaProduk = getIntent().getStringExtra("hrgaProduk");
        final String stokProduk = getIntent().getStringExtra("stokProduk");
        final String desBarang = getIntent().getStringExtra("desProduk");
        final String imageURL = getIntent().getStringExtra("gmbrProduk");
        final String id = getIntent().getStringExtra("idProduk");
        final String userIDProduk = getIntent().getStringExtra("userId");

        nmBarang.setText(namaproduk);
        hrgaBarang.setText(hargaProduk);
        stokBarang.setText(stokProduk);
        deskripProduk.setText(desBarang);
        listPembeli = new ArrayList<>();

        auth = FirebaseAuth.getInstance();
        userId = auth.getCurrentUser().getUid();
        Log.d("join", "nama  : "+userId);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Transaksi");

        mDatabaseReference.orderByChild("idBarang").equalTo(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Transaksi ts = ds.getValue(Transaksi.class);
                    String key = ts.getPembeli();
                    Log.d("join", "nama  : "+key);
                    DatabaseReference barang = FirebaseDatabase.getInstance().getReference("pengguna");
                    barang.orderByChild("userId").equalTo(key)
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for (DataSnapshot fs : dataSnapshot.getChildren()) {
                                        User beli = fs.getValue(User.class);
                                        listPembeli.add(beli);
                                    }
                                    for (int i = 0; i<listPembeli.size();i++){
                                        User jajal = listPembeli.get(i);
                                        Log.d("join", "nama  : "+jajal.getNamaBelakang());

                                    }
                                    PembeliList pembeliAdapter = new PembeliList(DetailProduk.this, listPembeli);
                                    pembeli.setAdapter(pembeliAdapter);
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        pembeli.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                User user = listPembeli.get(i);

                Intent intent = new Intent(getApplicationContext(),DetailPenjualanSaya.class);
                intent.putExtra("namaPembeliIntent",user.getNamaDepan()+" "+user.getNamaBelakang());
                intent.putExtra("alamatPembeliIntent", user.getAlamatLengkap()+", "+user.getKecamatan()+", "+user.getKabupaten()+", "+user.getProvinsi());
                intent.putExtra("idPembeliIntent",user.getUserId());
                startActivity(intent);
            }
        });


        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUpdateDeleteDialog(id, namaproduk, hargaProduk, stokProduk, desBarang, imageURL);
            }
        });


        Log.d("gambarku", "onCreate: " + imageURL);
        Log.d("idproduk", "onCreate: " + id);

        Picasso.get()
                .load(imageURL)
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(fotoDetail);

    }

    private void showUpdateDeleteDialog(final String id_produk, String nama_Produk, final String harga, final String stok, final String deskripsi, final String image) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.edit_produk, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.edtNamaProduk);
        final EditText editTextHarga = (EditText) dialogView.findViewById(R.id.edtHargaBarang);
        final EditText editTextStok = (EditText) dialogView.findViewById(R.id.edtStokBarang);
        final EditText editTextDeskrip = (EditText) dialogView.findViewById(R.id.edtDeskripsi);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.btn_edit_barang);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.btn_delete_barang);

        dialogBuilder.setTitle("Edit Produk" + " - " + nama_Produk);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        editTextName.setText(nama_Produk);
        editTextHarga.setText(harga);
        editTextStok.setText(stok);
        editTextDeskrip.setText(deskripsi);

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String namab = editTextName.getText().toString().trim();
                String hargab = editTextHarga.getText().toString().trim();
                String stokb = editTextStok.getText().toString().trim();
                String deskb = editTextDeskrip.getText().toString().trim();
                String berat = "1000";

                if (!TextUtils.isEmpty(namab)) {
                    updateProduk(id_produk, berat, namab, hargab, stokb, deskb, image);
                    nmBarang.setText(namab);
                    hrgaBarang.setText(hargab);
                    stokBarang.setText(stokb);
                    deskripProduk.setText(deskb);
                    b.dismiss();
                }
            }
        });


        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduk(id_produk);
                b.dismiss();
                Intent intent = new Intent(DetailProduk.this, HalamanUtama.class);
                startActivity(intent);


            }
        });

    }

    private boolean deleteProduk(String id) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("produk").child(id);

        //removing artist
        dR.removeValue();

        //getting the tracks reference for the specified artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("produk").child(id);

        //removing all tracks
        drTracks.removeValue();
        Toast.makeText(getApplicationContext(), "Produk Deleted", Toast.LENGTH_LONG).show();

        return true;
    }


    private boolean updateProduk(String id, String berat, String nama_produk, String stok, String harga, String deskripsi, String imgUrl) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("produk").child(id);

        //updating artist
        Produk produk = new Produk(id, berat, nama_produk, deskripsi, harga, stok, imgUrl,
                FirebaseAuth.getInstance().getCurrentUser().getUid());
        dR.setValue(produk);
        Toast.makeText(getApplicationContext(), "Produk Updated", Toast.LENGTH_LONG).show();
        return true;
    }
}
