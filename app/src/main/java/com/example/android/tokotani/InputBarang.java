package com.example.android.tokotani;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.android.tokotani.model.Produk;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class InputBarang extends AppCompatActivity {
    ImageView image;
    EditText namaProduk,deskripsiProduk,harga,stok;
    private static final int PICK_IMAGE = 100;

    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseReference;
    private FirebaseAuth mAuth;

    Uri imageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inputbarang);
        image = findViewById(R.id.input_gambar);
        namaProduk = findViewById(R.id.nama_produk_input);
        deskripsiProduk = findViewById(R.id.deskripsi_produk_input);
        harga = findViewById(R.id.harga_input);
        stok = findViewById(R.id.stok_input);
        getSupportActionBar().hide();

        mStorageRef = FirebaseStorage.getInstance().getReference("produk");
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("produk");
        mAuth = FirebaseAuth.getInstance();
    }

    public void SelectImage(View view){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode,data);
        if (resultCode== Activity.RESULT_OK&& requestCode ==  PICK_IMAGE){
            imageUri=data.getData();
            image.setImageURI(imageUri);
        }
    }
    private String getFileExtension(Uri uri){
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));
    }

    public void tambahMenu(View v){
        FirebaseUser user = mAuth.getCurrentUser();
        final String userId = user.getUid();
        final String berat = "1000";
        final StorageReference imageRef = mStorageRef.child(System.currentTimeMillis()+"."+getFileExtension(imageUri));
        imageRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Log.d("Upload Foto","onSuccess : uri "+uri.toString());
                        String urlImage = uri.toString().trim() ;
                        String UploadId = mDatabaseReference.push().getKey();
                        Produk produk = new Produk(UploadId,berat,namaProduk.getText().toString().trim(),deskripsiProduk.getText().toString().trim(),harga.getText().toString().trim(),stok.getText().toString().trim(),urlImage,userId);

                        mDatabaseReference.child(UploadId).setValue(produk);
                        startActivity(new Intent(InputBarang.this, HalamanUtama.class));
                    }
                });
                // Get a URL to the uploaded content


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        })
        ;
    }
}
