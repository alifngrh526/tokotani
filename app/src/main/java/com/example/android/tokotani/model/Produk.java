package com.example.android.tokotani.model;

public class Produk {
    private String berat,namaProduk,deskripsiProduk,hargaSatuan,stok,imageUrl,userId, id_produk,tagKab,tagProvinsi;

    public Produk(String id_produk,String berat, String namaProduk, String deskripsiProduk, String hargaSatuan, String stok, String imageUrl, String userId) {
        this.id_produk = id_produk;
        this.namaProduk = namaProduk;
        this.deskripsiProduk = deskripsiProduk;
        this.hargaSatuan = hargaSatuan;
        this.stok = stok;
        this.imageUrl = imageUrl;
        this.userId = userId;
        this.berat = berat;
    }

    public Produk(){
    }

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public void setId_produk(String id_produk) {
        this.id_produk = id_produk;
    }

    public String getId_produk() {
        return id_produk;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getDeskripsiProduk() {
        return deskripsiProduk;
    }

    public void setDeskripsiProduk(String deskripsiProduk) {
        this.deskripsiProduk = deskripsiProduk;
    }

    public String getHargaSatuan() {
        return hargaSatuan;
    }

    public void setHargaSatuan(String hargaSatuan) {
        this.hargaSatuan = hargaSatuan;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
