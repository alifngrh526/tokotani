package com.example.android.tokotani.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.tokotani.Login;
import com.example.android.tokotani.R;
import com.example.android.tokotani.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DataDiriSaya.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DataDiriSaya#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DataDiriSaya extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef,myRef1;
    private String userUID, emailInput, namaInput;
    ArrayList<User> akun;

    TextView email,nama,noTelp,alamatLengkap,kecamatan,kabupaten,kodepos, kategori;
    Button logout;

    public DataDiriSaya() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DataDiriSaya.
     */
    // TODO: Rename and change types and number of parameters
    public static DataDiriSaya newInstance(String param1, String param2) {
        DataDiriSaya fragment = new DataDiriSaya();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.info_datadiri,null);
        initializeView(rootView);

        return rootView;
    }
    private void initializeView(View rootView){

        email = rootView.findViewById(R.id.info_email_ds);
        nama = rootView.findViewById(R.id.info_nama_ds);
        noTelp = rootView.findViewById(R.id.info_notelp_ds);
        alamatLengkap = rootView.findViewById(R.id.indo_alamat_ds);
        kecamatan = rootView.findViewById(R.id.info_kecamatan_ds);
        kabupaten = rootView.findViewById(R.id.indo_kabupaten_ds);
        kodepos = rootView.findViewById(R.id.info_kodepos_ds);
        kategori = rootView.findViewById(R.id.info_kategori_ds);
        logout = rootView.findViewById(R.id.logout_ds);
        logout.setOnClickListener(this);
        akun = new ArrayList<>();



        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        userUID = user.getUid();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference("pengguna/"+userUID);


        emailInput = user.getEmail().trim();
        Log.d("jajal",emailInput);
//        namaInput =  user.getDisplayName();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("jajal", "onAuthStateChanged:signed_in:" + user.getUid());
//                    toastMessage("Successfully signed in with: " + user.getEmail());
                } else {
                    // User is signed out
                    Log.d("jajal", "onAuthStateChanged:signed_out");
//                    toastMessage("Successfully signed out.");
                }
                // ...
            }
        };

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showData(DataSnapshot dataSnapshot) {
        String id = myRef.push().getKey();
        Log.d("jajal" ,"id : "+id);

        User uInfo = dataSnapshot.getValue(User.class);
        akun.add(uInfo);

        for (int i = 0; i<akun.size();i++){
            User input = akun.get(i);
            Log.d("jajal","show data : notelp : "+input.getNoTelp());
            namaInput = input.getNamaDepan() +" "+ input.getNamaBelakang();
            String notelpAmbil = input.getNoTelp();
            String alamatAmbil = input.getAlamatLengkap();
            String kecamatanAmbil = input.getKecamatan();
            String kabupatenAmbil = input.getKabupaten();
            String kodeposAmbil = input.getKodepos();
            String kategoriAmbil = input.getKategori();

            email.setText(emailInput);
            nama.setText(namaInput);
            Log.d("jajal","nama : "+namaInput);
            noTelp.setText(notelpAmbil);
            alamatLengkap.setText(alamatAmbil);
            kecamatan.setText(kecamatanAmbil);
            kabupaten.setText(kabupatenAmbil);
            kodepos.setText(kodeposAmbil);
            kategori.setText(kategoriAmbil);
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        mAuth.signOut();
        startActivity(new Intent(getActivity().getApplicationContext(), Login.class));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
