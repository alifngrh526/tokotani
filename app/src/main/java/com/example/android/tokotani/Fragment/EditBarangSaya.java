package com.example.android.tokotani.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.tokotani.BeliProduk;
import com.example.android.tokotani.HalamanUtama;
import com.example.android.tokotani.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class EditBarangSaya extends AppCompatActivity {

    ImageView fotoDetail;
    TextView nmBarang,hrgaBarang,stokBarang,deskripProduk;
    //Button btEdit;
    String namaproduk,hargaProduk,stokProduk,desBarang,imageURL,id,produkId,stok;

    //private String namaProduk,imageURL,hargaProduk,stokProduk,desBarang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_barang_saya);

        nmBarang = findViewById(R.id.judulDetail);
        fotoDetail = findViewById(R.id.detailFoto);
        hrgaBarang = findViewById(R.id.hargaDetail);
        stokBarang = findViewById(R.id.stokDetail);
        deskripProduk = findViewById(R.id.deskripsiDetail);
        Button btn_edit = findViewById(R.id.btn_edit);

        produkId =  getIntent().getStringExtra("idProduk");
        namaproduk = getIntent().getStringExtra("nmProduk");
        hargaProduk =  getIntent().getStringExtra("hrgaProduk");
        stokProduk = getIntent().getStringExtra("stokProduk");
        desBarang = getIntent().getStringExtra("desProduk");
         imageURL = getIntent().getStringExtra("gmbrProduk");
          id = getIntent().getStringExtra("idProduk");
          stok = getIntent().getStringExtra("stokProduk");
        Button btn_beli = findViewById(R.id.btn_beli);

        nmBarang.setText(namaproduk);
        hrgaBarang.setText(hargaProduk);
        stokBarang.setText(stokProduk);
        deskripProduk.setText(desBarang);

        Log.d("harga", "harga: "+ hargaProduk);
        Log.d("idproduk", "onCreate: "+ id);

        Picasso.get()
                .load(imageURL)
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(fotoDetail);
        final String userIDProduk = getIntent().getStringExtra("userId");

        btn_beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditBarangSaya.this, BeliProduk.class);
                intent.putExtra("beliNama",namaproduk);
                intent.putExtra("beliharga",hargaProduk);
                intent.putExtra("beliUrl",imageURL);
                intent.putExtra("beliuserId",userIDProduk);
                intent.putExtra("beliprodukId",produkId);
                intent.putExtra("beliStokProduk",stok);
                intent.putExtra("beliDes",desBarang);

                startActivity(intent);
            }
        });
    }
    public void chat(View view) {
        String url = "https://api.whatsapp.com/send?phone=6283101135508&text=Halo" ;
        Intent bukabrowser = new Intent(Intent. ACTION_VIEW);
        bukabrowser.setData(Uri. parse(url));
        startActivity(bukabrowser);
    }

    private void showUpdateDeleteDialog(final String id_produk, String nama_Produk, final String harga, final String stok, final String deskripsi) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.edit_produk, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.edtNamaProduk);
        final EditText editTextHarga = (EditText) dialogView.findViewById(R.id.edtHargaBarang);
        final EditText editTextStok = (EditText) dialogView.findViewById(R.id.edtStokBarang);
        final EditText editTextDeskrip = (EditText) dialogView.findViewById(R.id.edtDeskripsi);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.btn_edit_barang);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.btn_delete_barang);
        Button btn_edit = findViewById(R.id.btn_edit);



        dialogBuilder.setTitle("Edit Produk" +" - "+ nama_Produk);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        editTextName.setText(nama_Produk);
        editTextHarga.setText(harga);
        editTextStok.setText(stok);
        editTextDeskrip.setText(deskripsi);


        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduk(id_produk);
                b.dismiss();
                Intent intent = new Intent(EditBarangSaya.this, HalamanUtama.class);
                startActivity(intent);


            }
        });

    }

    private boolean deleteProduk(String id) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("produk").child(id);

        //removing artist
        dR.removeValue();

        //getting the tracks reference for the specified artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("produk").child(id);

        //removing all tracks
        drTracks.removeValue();
        Toast.makeText(getApplicationContext(), "Produk Deleted", Toast.LENGTH_LONG).show();

        return true;
    }


}
