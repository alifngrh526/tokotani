package com.example.android.tokotani.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.android.tokotani.Fragment.BarangSaya;
import com.example.android.tokotani.Fragment.DataDiriSaya;

public class AdapterDatadiri extends FragmentStatePagerAdapter {
    int noTab;
    public AdapterDatadiri(FragmentManager fm, int tab) {
        super(fm);
        this.noTab = tab;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                DataDiriSaya barang1 = new DataDiriSaya();
                return barang1;
            case 1 :
                BarangSaya barangSaya =  new BarangSaya();
                return barangSaya;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return noTab;
    }
}
