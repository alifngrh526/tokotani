 package com.example.android.tokotani;

 import android.content.Context;
 import android.content.Intent;
 import android.os.Bundle;
 import android.support.annotation.NonNull;
 import android.support.v7.app.AppCompatActivity;
 import android.util.Patterns;
 import android.view.View;
 import android.widget.EditText;
 import android.widget.Toast;

 import com.google.android.gms.tasks.OnCompleteListener;
 import com.google.android.gms.tasks.Task;
 import com.google.firebase.FirebaseApp;
 import com.google.firebase.auth.AuthResult;
 import com.google.firebase.auth.FirebaseAuth;

 public class Login extends AppCompatActivity implements View.OnClickListener {
     private FirebaseAuth mAuth;
     Context contet = Login.this;
     EditText editTextemail, editTextpassword;
     @Override
     protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(contet);
        setContentView(R.layout.activity_login);
         getSupportActionBar().hide();


        mAuth = FirebaseAuth.getInstance();
        editTextemail = findViewById(R.id.editText_email);
        editTextpassword = findViewById(R.id.editText_KataSandi);

        findViewById(R.id.button_masuk).setOnClickListener(this);
     }
     public void pilihDaftar(View view){
        Intent signUp = new Intent(this,Daftar.class);
        startActivity(signUp);
     }

     @Override
     public void onClick(View v) {
         switch (v.getId()){
             case R.id.button_masuk:
                 loginUser();
                 break;
         }
     }

     void loginUser(){
         String emailUser = editTextemail.getText().toString().trim();
         String passwordUser = editTextpassword .getText().toString().trim();

         if (emailUser.isEmpty()){
             editTextemail.setError("Masukkan email");
             editTextemail.requestFocus();
             return;
         }
         if (!Patterns.EMAIL_ADDRESS.matcher(emailUser).matches()){
             editTextemail.setError("Masukkan email yang valid");
             editTextemail.requestFocus();
             return;
         }
         if (passwordUser.length()<6){
             editTextpassword.setError("Panjang password minimal 6 karakter");
             editTextpassword.requestFocus();
             return;
         }
         if (passwordUser.isEmpty()){
             editTextpassword.setError("Masukkan password");
             editTextpassword.requestFocus();
             editTextemail.requestFocus();
         }
         mAuth.signInWithEmailAndPassword(emailUser, passwordUser).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
             @Override
             public void onComplete(@NonNull Task<AuthResult> task) {
                 if (task.isSuccessful()){
                     Intent berhasilLogin = new Intent(Login.this, HalamanUtama.class);
                     berhasilLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                     startActivity(berhasilLogin);
                 }else{
                     Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                 }
             }
         });

     }

 }
