package com.example.android.tokotani.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.android.tokotani.HalamanUtama;
import com.example.android.tokotani.R;
import com.example.android.tokotani.adapter.AdapterBarangkuViewPager;

public class InfoAkun extends AppCompatActivity implements DataDiriSaya.OnFragmentInteractionListener, BarangSaya.OnFragmentInteractionListener {

    AdapterBarangkuViewPager adapterBarangkuViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datadiri_dan_barang);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabdatadiri);

        final ViewPager viewPager = (ViewPager)findViewById(R.id.viwpagerDatadiri);
        adapterBarangkuViewPager = new AdapterBarangkuViewPager(getSupportFragmentManager());

        adapterBarangkuViewPager.addFragment(new DataDiriSaya(),"Data Diri");
        adapterBarangkuViewPager.addFragment(new BarangSaya(),"Barang Jual Saya");
        adapterBarangkuViewPager.addFragment(new DataPembelianSaya(),"Pembelian Saya");

        viewPager.setAdapter(adapterBarangkuViewPager);
        tabLayout.setupWithViewPager(viewPager);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startActivity(new Intent(InfoAkun.this, HalamanUtama.class));
                    return true;
                case R.id.navigation_profile:
                    startActivity(new Intent(InfoAkun.this, InfoAkun.class));
                    return true;
            }
            return false;
        }
    };


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
