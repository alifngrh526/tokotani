package com.example.android.tokotani;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class Daftar extends AppCompatActivity implements View.OnClickListener{

    EditText editTextemail, editTextpassword;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        FirebaseApp.initializeApp(this);
        getSupportActionBar().hide();

        mAuth = FirebaseAuth.getInstance();

        editTextemail = findViewById(R.id.email);
        editTextpassword = findViewById(R.id.password);

        findViewById(R.id.button_daftar).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_daftar:
                regisUser();
                break;
        }
    }

    void regisUser(){
        String emailUser = editTextemail.getText().toString().trim();
        String passwordUser = editTextpassword .getText().toString().trim();

        if (emailUser.isEmpty()){
            editTextemail.setError("Masukkan email");
            editTextemail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailUser).matches()){
            editTextemail.setError("Masukkan email yang valid");
            editTextemail.requestFocus();
            return;
        }
        if (passwordUser.length()<6){
            editTextpassword.setError("Panjang password minimal 6 karakter");
            editTextpassword.requestFocus();
            return;
        }
        if (passwordUser.isEmpty()){
            editTextpassword.setError("Masukkan password");
            editTextpassword.requestFocus();
            editTextemail.requestFocus();
        }
        mAuth.createUserWithEmailAndPassword(emailUser, passwordUser)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(getApplicationContext(), "Berhasil menjadi user", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Daftar.this, InputDataDiri.class));
                        } else {
                            // If sign in fails, display a message to the user.
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                Toast.makeText(getApplicationContext(), "Kamu telah melakukan registrasi", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Gagal menambah user", Toast.LENGTH_SHORT).show();
                                Log.e("Signup Bosok", "Bosok Signup", task.getException());
                            }
                        }                        // ...
                    }
                });
    }

}
