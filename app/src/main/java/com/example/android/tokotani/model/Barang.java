package com.example.android.tokotani.model;

public class Barang {
    private String nama;
    private String harga;
    private String deskripsi;

    public String getNama() {
        return nama;
    }

    public String getHarga() {
        return harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public int getAvatar() {
        return avatar;
    }

    private final int avatar;

    public Barang (String nama, String harga, String deskripsi, int avatar) {
        this.nama = nama;
        this.harga = harga;
        this.deskripsi = deskripsi;
        this.avatar = avatar;
    }
}

