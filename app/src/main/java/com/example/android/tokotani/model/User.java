package com.example.android.tokotani.model;

public class User {
    String userId,email,namaDepan,namaBelakang,noTelp,alamatLengkap,
            kecamatan,kabupaten,provinsi,kodepos,kategori,tagKab,tagProv;

    public User() {
    }

    public User(String id,String email, String namaDepan, String namaBelakang,
                String noTelp, String alamatLengkap, String kecamatan,
                String kabupaten,String provinsi, String kodepos, String kategori, String tagKab, String tagProv) {
        this.userId = id;
        this.email = email;
        this.namaDepan = namaDepan;
        this.namaBelakang = namaBelakang;
        this.noTelp = noTelp;
        this.provinsi = provinsi;
        this.alamatLengkap = alamatLengkap;
        this.kecamatan = kecamatan;
        this.kabupaten = kabupaten;
        this.kodepos = kodepos;
        this.kategori = kategori;
        this.tagKab = tagKab;
        this.tagProv = tagProv;
    }

    public String getTagKab() {
        return tagKab;
    }

    public void setTagKab(String tagKab) {
        this.tagKab = tagKab;
    }

    public String getTagProv() {
        return tagProv;
    }

    public void setTagProv(String tagProv) {
        this.tagProv = tagProv;
    }

    public String getUserId() {
        return userId;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNamaDepan(String namaDepan) {
        this.namaDepan = namaDepan;
    }

    public void setNamaBelakang(String namaBelakang) {
        this.namaBelakang = namaBelakang;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public void setAlamatLengkap(String alamatLengkap) {
        this.alamatLengkap = alamatLengkap;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public void setKodepos(String kodepos) {
        this.kodepos = kodepos;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getEmail() {
        return email;
    }

    public String getNamaDepan() {
        return namaDepan;
    }

    public String getNamaBelakang() {
        return namaBelakang;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public String getAlamatLengkap() {
        return alamatLengkap;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public String getKodepos() {
        return kodepos;
    }

    public String getKategori() {
        return kategori;
    }
}
