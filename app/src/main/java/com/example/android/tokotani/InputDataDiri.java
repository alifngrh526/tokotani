package com.example.android.tokotani;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.tokotani.adapter.CityAdapter;
import com.example.android.tokotani.adapter.ProvinceAdapter;
import com.example.android.tokotani.api.ApiService;
import com.example.android.tokotani.api.ApiUrl;
import com.example.android.tokotani.model.User;
import com.example.android.tokotani.model.city.ItemCity;
import com.example.android.tokotani.model.province.ItemProvince;
import com.example.android.tokotani.model.province.Result;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import com.example.android.tokotani.model.city.Result;

public class InputDataDiri extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    String emailInput;
    String [] daftarKategori = new String[2];
    EditText email,namaDepan,namaBelakang,noTelp,alamatLengkap,kecamatan,kabupaten,kodepos,provinsi;
    Spinner kategori;
    Button inputData;
    private AlertDialog.Builder alert;
    private AlertDialog ad;
    private ListView mListView;
    private CityAdapter adapter_city;
    private List<com.example.android.tokotani.model.city.Result> ListCity = new ArrayList<com.example.android.tokotani.model.city.Result>();

    private ProvinceAdapter adapter_province;
    private List<Result> ListProvince = new ArrayList<Result>();

    DatabaseReference databaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_datadiri);
        email = findViewById(R.id.email_ds);
        namaDepan = findViewById(R.id.namadepan_ds);
        namaBelakang = findViewById(R.id.namabelakang_ds);
        noTelp =  findViewById(R.id.notelp_ds);
        alamatLengkap = findViewById(R.id.alamatlengkap_ds);
        kecamatan =  findViewById(R.id.kecamatan_ds);
        kabupaten =  findViewById(R.id.kabupaten_ds);
        provinsi = findViewById(R.id.provinsi_ds);
        kodepos =  findViewById(R.id.kodepos_ds);
        kategori = findViewById(R.id.kategori_ds);
        inputData = findViewById(R.id.inputdata_ds);
        getSupportActionBar().hide();

        daftarKategori[0]= "Pembeli";
        daftarKategori[1]= "Penjual";
        final ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,daftarKategori);
        kategori.setAdapter(adapter);

        user = FirebaseAuth.getInstance().getCurrentUser();
        emailInput = user.getEmail().trim();
        email.setText(emailInput);

        databaseUser = FirebaseDatabase.getInstance().getReference("pengguna");

        inputData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputDataDiri();
            }
        });
        provinsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpProvince(provinsi, kabupaten);

            }
        });
        kabupaten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (provinsi.getTag().equals("")) {
                        provinsi.setError("Please chooise your form province");
                    } else {
                        popUpCity(kabupaten, provinsi);
                    }

                } catch (NullPointerException e) {
                    provinsi.setError("Please chooise your form province");
                }

            }
        });
    }
    public void popUpProvince(final EditText etProvince, final EditText etCity ) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View alertLayout = inflater.inflate(R.layout.custom_dialog_search, null);

        alert = new AlertDialog.Builder(InputDataDiri.this);
        alert.setTitle("List ListProvince");
        alert.setMessage("select your province");
        alert.setView(alertLayout);
        alert.setCancelable(true);

        ad = alert.show();

        mListView = (ListView) alertLayout.findViewById(R.id.listItem);

        ListProvince.clear();
        adapter_province = new ProvinceAdapter(InputDataDiri.this, ListProvince);
        mListView.setClickable(true);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object o = mListView.getItemAtPosition(i);
                Result cn = (Result) o;

                etProvince.setError(null);
                etProvince.setText(cn.getProvince());
                etProvince.setTag(cn.getProvinceId());

                etCity.setText("");
                etCity.setTag("");

                ad.dismiss();
            }
        });

        getProvince();
    }

    public void popUpCity(final EditText etCity, final EditText etProvince) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View alertLayout = inflater.inflate(R.layout.custom_dialog_search, null);

        alert = new AlertDialog.Builder(InputDataDiri.this);
        alert.setTitle("List City");
        alert.setMessage("select your city");
        alert.setView(alertLayout);
        alert.setCancelable(true);

        ad = alert.show();


        mListView = (ListView) alertLayout.findViewById(R.id.listItem);

        ListCity.clear();
        adapter_city = new CityAdapter(InputDataDiri.this, ListCity);
        mListView.setClickable(true);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object o = mListView.getItemAtPosition(i);
                com.example.android.tokotani.model.city.Result cn = (com.example.android.tokotani.model.city.Result) o;

                etCity.setError(null);
                etCity.setText(cn.getCityName());
                etCity.setTag(cn.getCityId());

                ad.dismiss();
            }
        });

        getCity(etProvince.getTag().toString());

    }
    public void getProvince() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrl.URL_ROOT_HTTPS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<ItemProvince> call = service.getProvince();

        call.enqueue(new Callback<ItemProvince>() {
            @Override
            public void onResponse(Call<ItemProvince> call, Response<ItemProvince> response) {

                Log.v("wow", "json : " + new Gson().toJson(response));

                if (response.isSuccessful()) {

                    int count_data = response.body().getRajaongkir().getResults().size();
                    for (int a = 0; a <= count_data - 1; a++) {
                        Result itemProvince = new Result(
                                response.body().getRajaongkir().getResults().get(a).getProvinceId(),
                                response.body().getRajaongkir().getResults().get(a).getProvince()
                        );

                        ListProvince.add(itemProvince);
                        mListView.setAdapter(adapter_province);
                    }

                    adapter_province.setList(ListProvince);
                    adapter_province.filter("");

                } else {
                    String error = "Error Retrive Data from Server !!!";
                    Toast.makeText(InputDataDiri.this, error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ItemProvince> call, Throwable t) {
//                progressDialog.dismiss();
                Toast.makeText(InputDataDiri.this, "Message : Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void getCity(String id_province) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrl.URL_ROOT_HTTPS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<ItemCity> call = service.getCity(id_province);

        call.enqueue(new Callback<ItemCity>() {
            @Override
            public void onResponse(Call<ItemCity> call, Response<ItemCity> response) {

//                progressDialog.dismiss();
                Log.v("wow", "json : " + new Gson().toJson(response));

                if (response.isSuccessful()) {

                    int count_data = response.body().getRajaongkir().getResults().size();
                    for (int a = 0; a <= count_data - 1; a++) {
                        com.example.android.tokotani.model.city.Result itemProvince = new com.example.android.tokotani.model.city.Result(
                                response.body().getRajaongkir().getResults().get(a).getCityId(),
                                response.body().getRajaongkir().getResults().get(a).getProvinceId(),
                                response.body().getRajaongkir().getResults().get(a).getProvince(),
                                response.body().getRajaongkir().getResults().get(a).getType(),
                                response.body().getRajaongkir().getResults().get(a).getCityName(),
                                response.body().getRajaongkir().getResults().get(a).getPostalCode()
                        );

                        ListCity.add(itemProvince);
                        mListView.setAdapter(adapter_city);
                    }

                    adapter_city.setList(ListCity);
                    adapter_city.filter("");

                } else {
                    String error = "Error Retrive Data from Server !!!";
                    Toast.makeText(InputDataDiri.this, error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ItemCity> call, Throwable t) {
//                progressDialog.dismiss();
                Toast.makeText(InputDataDiri.this, "Message : Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void inputDataDiri(){
        String namaDepanIn = namaDepan.getText().toString().trim();
        String namaBelakangIn = namaBelakang.getText().toString().trim();
        String noTelpIn = noTelp.getText().toString().trim();
        String alamatIn = alamatLengkap.getText().toString().trim();
        String kecamatanIn = kecamatan.getText().toString().trim();
        String kabupatenIn = kabupaten.getText().toString().trim();
        String tagKabupaten = kabupaten.getTag().toString();
        String kodePosIn = kodepos.getText().toString().trim();
        String kategoriIn = daftarKategori[kategori.getSelectedItemPosition()];
        String provinsiIn = provinsi.getText().toString().trim();
        String tagProvinsi = provinsi.getTag().toString();
        if (namaDepanIn.isEmpty()){
            namaDepan.setError("Masukkan Data");
            namaDepan.requestFocus();
            return;
        }
        if (namaBelakangIn.isEmpty()){
            namaBelakang.setError("Masukkan Data");
            namaBelakang.requestFocus();
            return;
        }
        if (noTelpIn.isEmpty()){
            noTelp.setError("Masukkan Data");
            noTelp.requestFocus();
            return;
        }
        if (alamatIn.isEmpty()){
            alamatLengkap.setError("Masukkan Data");
            alamatLengkap.requestFocus();
            return;
        }
        if (kecamatanIn.isEmpty()){
            kecamatan.setError("Masukkan Data");
            kecamatan.requestFocus();
            return;
        }
        if (kabupatenIn.isEmpty()){
            kabupaten.setError("Masukkan Data");
            kabupaten.requestFocus();
            return;
        }
        if (kodePosIn.isEmpty()){
            kodepos.setError("Masukkan Data");
            kodepos.requestFocus();
            return;
        }
        if (provinsiIn.isEmpty()){
            kodepos.setError("Masukkan Data");
            kodepos.requestFocus();
            return;
        }

        String id = user.getUid();

        Log.d("jajal","id = "+id);

        User pengguna = new User();
        pengguna.setUserId(id)  ;
        pengguna.setEmail(emailInput);
        pengguna.setNamaDepan(namaDepanIn);
        pengguna.setNamaBelakang(namaBelakangIn);
        pengguna.setNoTelp(noTelpIn);
        pengguna.setAlamatLengkap(alamatIn);
        pengguna.setKecamatan(kecamatanIn);
        pengguna.setKabupaten(kabupatenIn);
        pengguna.setProvinsi(provinsiIn);
        pengguna.setKodepos(kodePosIn);
        pengguna.setKategori(kategoriIn);
        pengguna.setTagKab(tagKabupaten);
        pengguna.setTagProv(tagProvinsi);

        databaseUser.child(id).setValue(pengguna);
        Toast.makeText(this, "Berhasil input data diri", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(InputDataDiri.this, Login.class));
    }
}
