package com.example.android.tokotani.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.tokotani.R;
import com.example.android.tokotani.model.User;

import java.util.List;

public class PembeliList extends ArrayAdapter<User> {
    private Activity context;
    List<User> pembeli;

    public PembeliList(Activity context, List<User> artists) {
        super(context, R.layout.layout_artist_list, artists);
        this.context = context;
        this.pembeli = artists;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_artist_list, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewGenre = (TextView) listViewItem.findViewById(R.id.textViewGenre);

        User artist = pembeli.get(position);
        textViewName.setText(artist.getNamaDepan()+" "+artist.getNamaBelakang());
        textViewGenre.setText(artist.getKabupaten());

        return listViewItem;
    }
}
