package com.example.android.tokotani.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.tokotani.Fragment.EditBarangSaya;
import com.example.android.tokotani.R;
import com.example.android.tokotani.model.Produk;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterBarang extends RecyclerView.Adapter<AdapterBarang.ImageViewHolder> {

    Context context;
    public ArrayList<Produk> getProduks() {
        return produks;
    }
    ArrayList<Produk> produks;


    public AdapterBarang(Context contexts, ArrayList<Produk> uploads) {
        context = contexts;
        produks = uploads;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list, viewGroup, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int postition) {
        Produk produkKu = produks.get(postition);
        imageViewHolder.namaBarang.setText(produkKu.getNamaProduk());
        imageViewHolder.hargaBarang.setText(produkKu.getHargaSatuan());
        imageViewHolder.stok.setText(produkKu.getStok());
//        imageViewHolder.deskripsi.setText(produkKu.getDeskripsiProduk());
//        imageViewHolder.cardView.setCardBackgroundColor(Color.GREEN);
        Picasso.get()
                .load(produkKu.getImageUrl())
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(imageViewHolder.gambarBarang);
        Log.d("Gambar", "gambar : " + produkKu.getImageUrl());
        Log.d("Gambar", "gambar : " + produkKu.getNamaProduk());
        Log.d("Gambar", "gambar : " + produkKu.getStok());
        Log.d("Gambar", "gambar : " + produkKu.getHargaSatuan());
        Log.d("Gambar", "gambar : " + produkKu.getDeskripsiProduk());
    }


    @Override
    public int getItemCount() {
        return produks.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView namaBarang, hargaBarang, stok, deskripsi;
        public ImageView gambarBarang;
        public CardView cardView;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            namaBarang = itemView.findViewById(R.id.nama_beli);
            hargaBarang = itemView.findViewById(R.id.harga_beli);
            stok = itemView.findViewById(R.id.stok_hp_ds);
//            deskripsi = itemView.findViewById(R.id.desc_hp_ds);
            gambarBarang = itemView.findViewById(R.id.gambar_beli);
            cardView = itemView.findViewById(R.id.cardView);

            itemView.setOnClickListener(this);
        }

        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), EditBarangSaya.class);
            intent.putExtra("nmProduk", namaBarang.getText().toString());
            intent.putExtra("stokProduk", stok.getText().toString());
            intent.putExtra("hrgaProduk", hargaBarang.getText().toString());
            intent.putExtra("desProduk", getProduks().get(getAdapterPosition()).getDeskripsiProduk());
            intent.putExtra("idProduk", getProduks().get(getAdapterPosition()).getId_produk());
            intent.putExtra("gmbrProduk", getProduks().get(getAdapterPosition()).getImageUrl());
            intent.putExtra("userId",getProduks().get(getAdapterPosition()).getUserId());
            v.getContext().startActivity(intent);

            Log.d("test", "onClick: " + getProduks().get(getAdapterPosition()).getId_produk());
        }
    }
}
