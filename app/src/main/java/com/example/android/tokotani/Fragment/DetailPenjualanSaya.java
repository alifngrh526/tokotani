package com.example.android.tokotani.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.tokotani.R;
import com.example.android.tokotani.model.Transaksi;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DetailPenjualanSaya extends AppCompatActivity {
    TextView namaPembeli, jmlBeli, totalHarga, statusPembelian, alamatPembeli;
    Button konfirmasiPenerimaan;
    private RecyclerView mRecyclerView;
    private DatabaseReference mDatabaseReference;
    ArrayList<Transaksi> trans;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    String userId, jmlBarang, statusPembelianam, totalHargaam, idBarangam, idTransaksiam, pembeliam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_penjualan_saya);
        namaPembeli =  findViewById(R.id.nama_pembeli_detail);
        alamatPembeli = findViewById(R.id.alamat_pembeli_detail);
        jmlBeli = findViewById(R.id.jumlahPembelianDetail_pnjl);
        totalHarga = findViewById(R.id.total_harga_ds_pnjl);
        statusPembelian = findViewById(R.id.statusPembelian_ds_pnjl);
        konfirmasiPenerimaan = findViewById(R.id.konfirmasi_pengiriman);
        String namaPembeliInput = getIntent().getStringExtra("namaPembeliIntent");
        String alamatPembeliInput = getIntent().getStringExtra("alamatPembeliIntent");
        String idUser = getIntent().getStringExtra("idPembeliIntent");
        namaPembeli.setText(namaPembeliInput);
        alamatPembeli.setText(alamatPembeliInput);
        Log.d("detailpmbl", "id : " + idUser);
        trans = new ArrayList<>();

        auth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("jajal", "onAuthStateChanged:signed_in:" + user.getUid());
//                    toastMessage("Successfully signed in with: " + user.getEmail());
                } else {
                    // User is signed out
                    Log.d("jajal", "onAuthStateChanged:signed_out");
//                    toastMessage("Successfully signed out.");
                }
                // ...
            }
        };
        userId = auth.getCurrentUser().getUid();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Transaksi");
        mDatabaseReference.orderByChild("pembeli").equalTo(idUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Transaksi transaksi = ds.getValue(Transaksi.class);
                    trans.add(transaksi);

                    for (int i = 0; i < trans.size(); i++) {
                        Transaksi ambil = trans.get(i);
                        jmlBarang = ambil.getJumlahBeli();
                        totalHargaam = ambil.getTotalHarga();
                        statusPembelianam = ambil.getStatusBarang();
                        idBarangam = ambil.getIdBarang();
                        idTransaksiam = ambil.getIdTransaksi();
                        pembeliam = ambil.getPembeli();
                        jmlBeli.setText(jmlBarang);
                        totalHarga.setText(totalHargaam);
                        statusPembelian.setText(statusPembelianam);
                    }
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        konfirmasiPenerimaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = "Barang telah Dikirim";
                updateTransaksi(status, idTransaksiam, pembeliam, idBarangam, jmlBarang, totalHargaam);
                statusPembelian.setText(status);
            }
        });
    }

    public void Konfirmasi(View view) {
        String status = "Barang telah Diterima";
        updateTransaksi(status, idTransaksiam, pembeliam, idBarangam, jmlBarang, totalHargaam);
        statusPembelian.setText(status);
    }

    private boolean updateTransaksi(String status, String idtrans, String pembeli, String idBarang, String jumlahBeli, String totalHarga) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("Transaksi").child(idtrans);

        //updating artist
        Transaksi transaksi = new Transaksi(status, idtrans, pembeli, idBarang, jumlahBeli, totalHarga);
        dR.setValue(transaksi);
        Toast.makeText(getApplicationContext(), "Produk Updated", Toast.LENGTH_LONG).show();
        return true;
    }
}
