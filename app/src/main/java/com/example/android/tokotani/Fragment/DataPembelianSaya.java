package com.example.android.tokotani.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.tokotani.R;
import com.example.android.tokotani.adapter.AdapterBarangBeli;
import com.example.android.tokotani.model.Produk;
import com.example.android.tokotani.model.Transaksi;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DataPembelianSaya extends Fragment {

    private RecyclerView mRecyclerView;
    private DatabaseReference mDatabaseReference;
    private ArrayList<Produk> produk;
    private AdapterBarangBeli mAdapter;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    String userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_data_jual, null);
        initializeView(rootView);
        return rootView;

    }

    public void initializeView(View rootView) {
        mRecyclerView = rootView.findViewById(R.id.recycleJual);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        auth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("jajal", "onAuthStateChanged:signed_in:" + user.getUid());
//                    toastMessage("Successfully signed in with: " + user.getEmail());
                } else {
                    // User is signed out
                    Log.d("jajal", "onAuthStateChanged:signed_out");
//                    toastMessage("Successfully signed out.");
                }
                // ...
            }
        };
        userId = auth.getCurrentUser().getUid();
        produk = new ArrayList<>();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("Transaksi");

        mDatabaseReference.orderByChild("pembeli").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Transaksi ts = ds.getValue(Transaksi.class);
                    String key = ts.getIdBarang();

                    DatabaseReference barang = FirebaseDatabase.getInstance().getReference("produk");
                    barang.orderByChild("id_produk").equalTo(key)
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for (DataSnapshot fs : dataSnapshot.getChildren()) {
                                        Produk beli = fs.getValue(Produk.class);
                                        produk.add(beli);
                                    }
                                    mAdapter = new AdapterBarangBeli(getActivity(), produk);
                                    mRecyclerView.setAdapter(mAdapter);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
