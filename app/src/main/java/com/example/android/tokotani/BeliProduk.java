package com.example.android.tokotani;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.tokotani.adapter.BarangAdapter;
import com.example.android.tokotani.adapter.ExpedisiAdapter;
import com.example.android.tokotani.api.ApiService;
import com.example.android.tokotani.api.ApiUrl;
import com.example.android.tokotani.model.Produk;
import com.example.android.tokotani.model.Transaksi;
import com.example.android.tokotani.model.User;
import com.example.android.tokotani.model.cost.ItemCost;
import com.example.android.tokotani.model.expedisi.ItemExpedisi;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import com.bagicode.cekongkir.model.city.ItemCity;
//import com.bagicode.cekongkir.model.province.ItemProvince;

public class BeliProduk extends AppCompatActivity {
    private EditText etFromProvince, etToProvince;
    private EditText etFromCity, etToCity;
    private EditText etWeight, etCourier;
    private EditText banyakBrng;
    private ImageView gmbrBeli;
    private TextView hargaKurir, nmBeli, hrgBeli, ttl;
    private ProgressDialog progressDialog;

    private AlertDialog.Builder alert;
    private AlertDialog ad;
    private EditText searchList;
    private ListView mListView;
    String bnykbrng;
    private ExpedisiAdapter adapter_expedisi;
    private List<ItemExpedisi> listItemExpedisi = new ArrayList<ItemExpedisi>();

    private DatabaseReference mDatabaseReference;
    private DatabaseReference mDatabaseReference1;
    private DatabaseReference mDatabaseReference2;
    private DatabaseReference mDatabaseReference3;
    private ArrayList<User> pengguna;
    private ArrayList<User> pembeli;
    private ArrayList<Produk> produk;
    private BarangAdapter mAdapter;
    private FirebaseAuth auth;
    private String userId;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private String prdkId, berat, hargaBeli,stokSaya,namaBeli,urlBeli,des;
    private int totalHrga;
    private int totalBerat;
    private String userIdBeli;
    private Button lanjut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beli_produk);

        etFromProvince = (EditText) findViewById(R.id.etFromProvince);
        etFromCity = (EditText) findViewById(R.id.etFromCity);
        etToProvince = (EditText) findViewById(R.id.etToProvince);
        etToCity = (EditText) findViewById(R.id.etToCity);
        etWeight = (EditText) findViewById(R.id.etWeight);
        etCourier = (EditText) findViewById(R.id.etCourier);
        banyakBrng = findViewById(R.id.banyakBarang);
        hargaKurir = findViewById(R.id.hargaKurir);
        nmBeli = findViewById(R.id.nama_beli);
        hrgBeli = findViewById(R.id.harga_beli);
        gmbrBeli = findViewById(R.id.gambar_beli);
        ttl = findViewById(R.id.totalHarga);
        lanjut = findViewById(R.id.buttonLanjut);

        namaBeli = getIntent().getStringExtra("beliNama");
        hargaBeli = getIntent().getStringExtra("beliharga");
        urlBeli = getIntent().getStringExtra("beliUrl");
        userIdBeli = getIntent().getStringExtra("beliuserId");
        prdkId = getIntent().getStringExtra("beliprodukId");
        stokSaya = getIntent().getStringExtra("beliStokProduk");
        des = getIntent().getStringExtra("beliDes");

        nmBeli.setText(namaBeli);
        hrgBeli.setText(hargaBeli);
        Log.d("beli", "id : " + hargaBeli);
        Log.d("beli", "id : " + userIdBeli);
        Log.d("beli", "id : " + prdkId);
        Picasso.get()
                .load(urlBeli)
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(gmbrBeli);

        pengguna = new ArrayList<>();
        pembeli = new ArrayList<>();
        produk = new ArrayList<>();

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("pengguna/" + userIdBeli);
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User ambil = dataSnapshot.getValue(User.class);
                pengguna.add(ambil);
                for (int i = 0; i < pengguna.size(); i++) {
                    User jupuk = pengguna.get(i);
                    Log.d("kabupaten", "kabupaten : " + jupuk.getKabupaten());
                    etFromCity.setText(jupuk.getKabupaten());
                    etFromProvince.setText(jupuk.getProvinsi());
                    etFromProvince.setTag(jupuk.getTagProv());
                    etFromCity.setTag(jupuk.getTagKab());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        auth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("jajal", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("jajal", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        userId = auth.getCurrentUser().getUid();
        mDatabaseReference1 = FirebaseDatabase.getInstance().getReference("pengguna/" + userId);
        mDatabaseReference1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User ambil = dataSnapshot.getValue(User.class);
                pembeli.add(ambil);
                for (int i = 0; i < pembeli.size(); i++) {
                    User jupuk = pembeli.get(i);
                    Log.d("kabupaten", "kabupaten : " + jupuk.getKabupaten());
                    etToCity.setText(jupuk.getKabupaten());
                    etToCity.setTag(jupuk.getTagKab());
                    etToProvince.setText(jupuk.getProvinsi());
                    etToProvince.setTag(jupuk.getTagProv());
                    Log.d("beli", "id : " + jupuk.getTagKab());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mDatabaseReference2 = FirebaseDatabase.getInstance().getReference("produk/" + prdkId);

        mDatabaseReference2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Produk output = dataSnapshot.getValue(Produk.class);
                produk.add(output);
                for (int i = 0; i < produk.size(); i++) {
                    Produk produks = produk.get(i);
                    berat = produks.getBerat();
                    etWeight.setText(berat);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        etCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpExpedisi(etCourier);
            }
        });

        etCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpExpedisi(etCourier);
            }
        });

        Button btnProcess = (Button) findViewById(R.id.btnProcess);
        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Origin = etFromCity.getText().toString();
                String destination = etToCity.getText().toString();
                String Weight = etWeight.getText().toString();
                String expedisi = etCourier.getText().toString();

                if (Origin.equals("")) {
                    etFromCity.setError("Please input your origin");
                } else if (destination.equals("")) {
                    etToCity.setError("Please input your destination");
                } else if (Weight.equals("")) {
                    etWeight.setError("Please input your Weight");
                } else if (expedisi.equals("")) {
                    etCourier.setError("Please input your ItemExpedisi");
                } else {

                    progressDialog = new ProgressDialog(BeliProduk.this);
                    progressDialog.setMessage("Please wait..");
                    progressDialog.show();

                    getCoast(
                            etFromCity.getTag().toString(),
                            etToCity.getTag().toString(),
                            etWeight.getText().toString(),
                            etCourier.getText().toString()
                    );
                }

            }
        });
        lanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransaksiMasuk();
            }
        });

    }

    public void popUpExpedisi(final EditText etExpedisi) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View alertLayout = inflater.inflate(R.layout.custom_dialog_search, null);

        alert = new AlertDialog.Builder(BeliProduk.this);
        alert.setTitle("List Expedisi");
        alert.setMessage("select your Expedisi");
        alert.setView(alertLayout);
        alert.setCancelable(true);

        ad = alert.show();

        searchList = (EditText) alertLayout.findViewById(R.id.searchItem);
        searchList.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        mListView = (ListView) alertLayout.findViewById(R.id.listItem);

        listItemExpedisi.clear();
        adapter_expedisi = new ExpedisiAdapter(BeliProduk.this, listItemExpedisi);
        mListView.setClickable(true);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object o = mListView.getItemAtPosition(i);
                ItemExpedisi cn = (ItemExpedisi) o;

                etExpedisi.setError(null);
                etExpedisi.setText(cn.getName());
                etExpedisi.setTag(cn.getId());

                ad.dismiss();
            }
        });

        getExpedisi();


    }

    private void getExpedisi() {

        ItemExpedisi itemItemExpedisi = new ItemExpedisi();

        itemItemExpedisi = new ItemExpedisi("1", "pos");
        listItemExpedisi.add(itemItemExpedisi);
        itemItemExpedisi = new ItemExpedisi("1", "tiki");
        listItemExpedisi.add(itemItemExpedisi);
        itemItemExpedisi = new ItemExpedisi("1", "jne");
        listItemExpedisi.add(itemItemExpedisi);

        mListView.setAdapter(adapter_expedisi);

        adapter_expedisi.setList(listItemExpedisi);
        adapter_expedisi.filter("");
    }

    public void getCoast(String origin,
                         String destination,
                         String weight,
                         String courier) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrl.URL_ROOT_HTTPS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<ItemCost> call = service.getCost(
                "3c140618aaf850cfcdd7535689971d74",
                origin,
                destination,
                weight,
                courier
        );

        call.enqueue(new Callback<ItemCost>() {
            @Override
            public void onResponse(Call<ItemCost> call, Response<ItemCost> response) {

                Log.v("wow", "json : " + new Gson().toJson(response));
                progressDialog.dismiss();

                if (response.isSuccessful()) {

                    int statusCode = response.body().getRajaongkir().getStatus().getCode();

                    if (statusCode == 200) {
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        String hrgaCourir = response.body().getRajaongkir().getResults().get(0).getCosts().get(0).getCost().get(0).getValue().toString();
                        bnykbrng = banyakBrng.getText().toString();
                        if (bnykbrng.isEmpty()){
                            banyakBrng.setError("Masukkan Jumlah Barang");
                            banyakBrng.requestFocus();
                            return;
                        }else {
                            totalBerat = Integer.parseInt(hrgaCourir)*Integer.parseInt(bnykbrng);
                            Log.d("berat", "berat : " + totalBerat);
                        }
                        hargaKurir.setText("Rp. "+Integer.toString(totalBerat));

                        totalHrga = (Integer.parseInt(hargaBeli)*Integer.parseInt(bnykbrng))+totalBerat;
                        ttl.setText(Integer.toString(totalHrga));

                    } else {

                        String message = response.body().getRajaongkir().getStatus().getDescription();
                        Toast.makeText(BeliProduk.this, message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    String error = "Error Retrive Data from Server !!!";
                    Toast.makeText(BeliProduk.this, error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ItemCost> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(BeliProduk.this, "Message : Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void TransaksiMasuk(){
        mDatabaseReference3 = FirebaseDatabase.getInstance().getReference("Transaksi");
        String pembeliX = userId;
        String idBarangX = prdkId;
        String jumlahBeliX = bnykbrng;
        String totalHargaX = Integer.toString(totalHrga);
        String idTransaksiX = mDatabaseReference3.push().getKey();
        String statusBarang = "Belum Dikonfirmasi";
        String beratX = "1000";
        String stokUpdate = Integer.toString(Integer.parseInt(stokSaya)-Integer.parseInt(jumlahBeliX));
        Log.d("stok", "onCreate: "+ stokUpdate);

        Transaksi transaksi = new Transaksi();
        transaksi.setIdBarang(idBarangX);
        transaksi.setJumlahBeli(jumlahBeliX);
        transaksi.setPembeli(pembeliX);
        transaksi.setTotalHarga(totalHargaX);
        transaksi.setIdTransaksi(idTransaksiX);
        transaksi.setStatusBarang(statusBarang);

        mDatabaseReference3.child(idTransaksiX).setValue(transaksi);
        DatabaseReference update = FirebaseDatabase.getInstance().getReference("produk").child(idBarangX);
        Produk produk = new Produk(idBarangX, beratX, namaBeli, des, hargaBeli, stokUpdate, urlBeli,
                userIdBeli);
        update.setValue(produk);
        startActivity(new Intent(BeliProduk.this, HalamanUtama.class));

    }

}
