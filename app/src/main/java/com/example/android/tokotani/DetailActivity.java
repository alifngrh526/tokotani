package com.example.android.tokotani;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    private TextView DetailNama, DetailHarga, DetailDeskripsi;
    private ImageView DetailFoto;
    private int KodeFoto;
    private String mNama, mHarga, mDeskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        DetailNama = findViewById(R.id.TextView_DetailNama);
        DetailHarga = findViewById(R.id.TextView_DetailHarga);
        DetailDeskripsi = findViewById(R.id.TextView_DetailDeskripsi);
        DetailFoto = findViewById(R.id.ig_detail_Avatar);

        mNama = getIntent().getStringExtra("nama");
        mHarga = getIntent().getStringExtra("harga");
        mDeskripsi = getIntent().getStringExtra("deskripsi");

        DetailNama.setText(mNama);
        DetailHarga.setText(mHarga);
        DetailDeskripsi.setText(mDeskripsi);
    }
}
