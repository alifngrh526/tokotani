package com.example.android.tokotani.model;

public class Transaksi {
    String idTransaksi, pembeli,idBarang,jumlahBeli,totalHarga, statusBarang;

    public Transaksi(String status,String idTransaksi, String pembeli, String idBarang, String jumlahBeli, String totalHarga) {
        this.idTransaksi = idTransaksi;
        this.pembeli = pembeli;
        this.idBarang = idBarang;
        this.jumlahBeli = jumlahBeli;
        this.totalHarga = totalHarga;
        this.statusBarang = status;
    }

    public Transaksi() {
    }

    public String getStatusBarang() {
        return statusBarang;
    }

    public void setStatusBarang(String statusBarang) {
        this.statusBarang = statusBarang;
    }

    public String getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(String idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public String getPembeli() {
        return pembeli;
    }

    public void setPembeli(String pembeli) {
        this.pembeli = pembeli;
    }

    public String getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(String idBarang) {
        this.idBarang = idBarang;
    }

    public String getJumlahBeli() {
        return jumlahBeli;
    }

    public void setJumlahBeli(String jumlahBeli) {
        this.jumlahBeli = jumlahBeli;
    }

    public String getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(String totalHarga) {
        this.totalHarga = totalHarga;
    }
}
